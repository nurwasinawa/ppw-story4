from django.urls import path
from django.shortcuts import render
from .views import *
from story_4 import views

urlpatterns = [
    path('', views.index, name='home'),
    path('contact', views.contact, name='contact'),
    path('about', views.about, name='about'),
	path('gallery', views.gallery, name='gallery')
]
